package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.security.InvalidParameterException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.json.JSONException;
import org.junit.BeforeClass;
import org.junit.Test;

import tomatosir.weather.JsonWdParser;
import tomatosir.weather.WDParser;
import tomatosir.weather.WeatherData;

public class JsonWdParserTest {

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @Test
  public void testParseWeatherDataPositive() throws JSONException, Exception {
    WDParser parser = new JsonWdParser();
    WeatherData expected = new WeatherData.WeatherDataBuilder()
        .dateOfMeasurement(
            ZonedDateTime.of(2016, 8, 31, 15, 34, 17, 0, ZoneId.of("+2")))
        .feelsLikeTempCelsius(26).humidity(47).pressure(1024).pressureTrend("0")
        .tempCelsius(24.6).visibilityKM(10.0).windKPH(4.0).build();
    String jsonString = "{\n  \"response\": {\n  \"version\":\"0.1\",\n  \"termsofService\":\"http://www.wunderground.com/weather/api/d/terms.html\",\n  \"features\": {\n  \"conditions\": 1\n  }\n\t}\n  ,\t\"current_observation\": {\n\t\t\"image\": {\n\t\t\"url\":\"http://icons.wxug.com/graphics/wu2/logo_130x80.png\",\n\t\t\"title\":\"Weather Underground\",\n\t\t\"link\":\"http://www.wunderground.com\"\n\t\t},\n\t\t\"display_location\": {\n\t\t\"full\":\"Warsaw, Poland\",\n\t\t\"city\":\"Warsaw\",\n\t\t\"state\":\"\",\n\t\t\"state_name\":\"Poland\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"zip\":\"00000\",\n\t\t\"magic\":\"1\",\n\t\t\"wmo\":\"12375\",\n\t\t\"latitude\":\"52.16999817\",\n\t\t\"longitude\":\"20.96999931\",\n\t\t\"elevation\":\"107.00000000\"\n\t\t},\n\t\t\"observation_location\": {\n\t\t\"full\":\"KÅ\u0082obucka, Warszawa, \",\n\t\t\"city\":\"KÅ\u0082obucka, Warszawa\",\n\t\t\"state\":\"\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"latitude\":\"52.167099\",\n\t\t\"longitude\":\"20.992540\",\n\t\t\"elevation\":\"0 ft\"\n\t\t},\n\t\t\"estimated\": {\n\t\t},\n\t\t\"station_id\":\"IWARSZAW528\",\n\t\t\"observation_time\":\"Last Updated on August 31, 3:34 PM CEST\",\n\t\t\"observation_time_rfc822\":\"Wed, 31 Aug 2016 15:34:17 +0200\",\n\t\t\"observation_epoch\":\"1472650457\",\n\t\t\"local_time_rfc822\":\"Wed, 31 Aug 2016 15:37:35 +0200\",\n\t\t\"local_epoch\":\"1472650655\",\n\t\t\"local_tz_short\":\"CEST\",\n\t\t\"local_tz_long\":\"Europe/Warsaw\",\n\t\t\"local_tz_offset\":\"+0200\",\n\t\t\"weather\":\"Scattered Clouds\",\n\t\t\"temperature_string\":\"76.3 F (24.6 C)\",\n\t\t\"temp_f\":76.3,\n\t\t\"temp_c\":24.6,\n\t\t\"relative_humidity\":\"47%\",\n\t\t\"wind_string\":\"From the SW at 2.5 MPH Gusting to 5.0 MPH\",\n\t\t\"wind_dir\":\"SW\",\n\t\t\"wind_degrees\":225,\n\t\t\"wind_mph\":2.5,\n\t\t\"wind_gust_mph\":\"5.0\",\n\t\t\"wind_kph\":4.0,\n\t\t\"wind_gust_kph\":\"8.0\",\n\t\t\"pressure_mb\":\"1024\",\n\t\t\"pressure_in\":\"30.24\",\n\t\t\"pressure_trend\":\"0\",\n\t\t\"dewpoint_string\":\"55 F (13 C)\",\n\t\t\"dewpoint_f\":55,\n\t\t\"dewpoint_c\":13,\n\t\t\"heat_index_string\":\"NA\",\n\t\t\"heat_index_f\":\"NA\",\n\t\t\"heat_index_c\":\"NA\",\n\t\t\"windchill_string\":\"NA\",\n\t\t\"windchill_f\":\"NA\",\n\t\t\"windchill_c\":\"NA\",\n\t\t\"feelslike_string\":\"76.3 F (26 C)\",\n\t\t\"feelslike_f\":\"76.3\",\n\t\t\"feelslike_c\":\"26\",\n\t\t\"visibility_mi\":\"6.2\",\n\t\t\"visibility_km\":\"10.0\",\n\t\t\"solarradiation\":\"--\",\n\t\t\"UV\":\"3\",\"precip_1hr_string\":\"-999.00 in ( 0 mm)\",\n\t\t\"precip_1hr_in\":\"-999.00\",\n\t\t\"precip_1hr_metric\":\" 0\",\n\t\t\"precip_today_string\":\"0.00 in (0 mm)\",\n\t\t\"precip_today_in\":\"0.00\",\n\t\t\"precip_today_metric\":\"0\",\n\t\t\"icon\":\"partlycloudy\",\n\t\t\"icon_url\":\"http://icons.wxug.com/i/c/k/partlycloudy.gif\",\n\t\t\"forecast_url\":\"http://www.wunderground.com/global/stations/12375.html\",\n\t\t\"history_url\":\"http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=IWARSZAW528\",\n\t\t\"ob_url\":\"http://www.wunderground.com/cgi-bin/findweather/getForecast?query=52.167099,20.992540\",\n\t\t\"nowcast\":\"\"\n\t}\n}";
    WeatherData actual = parser.parseWD(jsonString);

    assertEquals(expected, actual);
  }

  @Test
  public void testParseWeatherDataNegative() throws JSONException, Exception {
    // Different unexpected weather data
    WeatherData unexpected = new WeatherData.WeatherDataBuilder()
        .dateOfMeasurement(
            ZonedDateTime.of(2016, 8, 31, 15, 34, 17, 0, ZoneId.of("+2")))
        .feelsLikeTempCelsius(26).humidity(30).pressure(1024).pressureTrend("+")
        .tempCelsius(20.0).visibilityKM(18).windKPH(4.0).build();

    String jsonString = "{\n  \"response\": {\n  \"version\":\"0.1\",\n  \"termsofService\":\"http://www.wunderground.com/weather/api/d/terms.html\",\n  \"features\": {\n  \"conditions\": 1\n  }\n\t}\n  ,\t\"current_observation\": {\n\t\t\"image\": {\n\t\t\"url\":\"http://icons.wxug.com/graphics/wu2/logo_130x80.png\",\n\t\t\"title\":\"Weather Underground\",\n\t\t\"link\":\"http://www.wunderground.com\"\n\t\t},\n\t\t\"display_location\": {\n\t\t\"full\":\"Warsaw, Poland\",\n\t\t\"city\":\"Warsaw\",\n\t\t\"state\":\"\",\n\t\t\"state_name\":\"Poland\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"zip\":\"00000\",\n\t\t\"magic\":\"1\",\n\t\t\"wmo\":\"12375\",\n\t\t\"latitude\":\"52.16999817\",\n\t\t\"longitude\":\"20.96999931\",\n\t\t\"elevation\":\"107.00000000\"\n\t\t},\n\t\t\"observation_location\": {\n\t\t\"full\":\"KÅ\u0082obucka, Warszawa, \",\n\t\t\"city\":\"KÅ\u0082obucka, Warszawa\",\n\t\t\"state\":\"\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"latitude\":\"52.167099\",\n\t\t\"longitude\":\"20.992540\",\n\t\t\"elevation\":\"0 ft\"\n\t\t},\n\t\t\"estimated\": {\n\t\t},\n\t\t\"station_id\":\"IWARSZAW528\",\n\t\t\"observation_time\":\"Last Updated on August 31, 3:34 PM CEST\",\n\t\t\"observation_time_rfc822\":\"Wed, 31 Aug 2016 15:34:17 +0200\",\n\t\t\"observation_epoch\":\"1472650457\",\n\t\t\"local_time_rfc822\":\"Wed, 31 Aug 2016 15:37:35 +0200\",\n\t\t\"local_epoch\":\"1472650655\",\n\t\t\"local_tz_short\":\"CEST\",\n\t\t\"local_tz_long\":\"Europe/Warsaw\",\n\t\t\"local_tz_offset\":\"+0200\",\n\t\t\"weather\":\"Scattered Clouds\",\n\t\t\"temperature_string\":\"76.3 F (24.6 C)\",\n\t\t\"temp_f\":76.3,\n\t\t\"temp_c\":24.6,\n\t\t\"relative_humidity\":\"47%\",\n\t\t\"wind_string\":\"From the SW at 2.5 MPH Gusting to 5.0 MPH\",\n\t\t\"wind_dir\":\"SW\",\n\t\t\"wind_degrees\":225,\n\t\t\"wind_mph\":2.5,\n\t\t\"wind_gust_mph\":\"5.0\",\n\t\t\"wind_kph\":4.0,\n\t\t\"wind_gust_kph\":\"8.0\",\n\t\t\"pressure_mb\":\"1024\",\n\t\t\"pressure_in\":\"30.24\",\n\t\t\"pressure_trend\":\"0\",\n\t\t\"dewpoint_string\":\"55 F (13 C)\",\n\t\t\"dewpoint_f\":55,\n\t\t\"dewpoint_c\":13,\n\t\t\"heat_index_string\":\"NA\",\n\t\t\"heat_index_f\":\"NA\",\n\t\t\"heat_index_c\":\"NA\",\n\t\t\"windchill_string\":\"NA\",\n\t\t\"windchill_f\":\"NA\",\n\t\t\"windchill_c\":\"NA\",\n\t\t\"feelslike_string\":\"76.3 F (26 C)\",\n\t\t\"feelslike_f\":\"76.3\",\n\t\t\"feelslike_c\":\"26\",\n\t\t\"visibility_mi\":\"6.2\",\n\t\t\"visibility_km\":\"10.0\",\n\t\t\"solarradiation\":\"--\",\n\t\t\"UV\":\"3\",\"precip_1hr_string\":\"-999.00 in ( 0 mm)\",\n\t\t\"precip_1hr_in\":\"-999.00\",\n\t\t\"precip_1hr_metric\":\" 0\",\n\t\t\"precip_today_string\":\"0.00 in (0 mm)\",\n\t\t\"precip_today_in\":\"0.00\",\n\t\t\"precip_today_metric\":\"0\",\n\t\t\"icon\":\"partlycloudy\",\n\t\t\"icon_url\":\"http://icons.wxug.com/i/c/k/partlycloudy.gif\",\n\t\t\"forecast_url\":\"http://www.wunderground.com/global/stations/12375.html\",\n\t\t\"history_url\":\"http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=IWARSZAW528\",\n\t\t\"ob_url\":\"http://www.wunderground.com/cgi-bin/findweather/getForecast?query=52.167099,20.992540\",\n\t\t\"nowcast\":\"\"\n\t}\n}";
    WDParser parser = new JsonWdParser();
    WeatherData actual = parser.parseWD(jsonString);

    assertNotEquals(unexpected, actual);
  }

  @Test(expected = JSONException.class)
  public void testThrowsExceptionOnWrongJson() throws JSONException,Exception {

    // WRONG JSON WITH version 0.1 missing : and missing value wind_kph
    String jsonString = "{\n  \"response\": {\n  \"version\"\"0.1\",\n  \"termsofService\":\"http://www.wunderground.com/weather/api/d/terms.html\",\n  \"features\": {\n  \"conditions\": 1\n  }\n\t}\n  ,\t\"current_observation\": {\n\t\t\"image\": {\n\t\t\"url\":\"http://icons.wxug.com/graphics/wu2/logo_130x80.png\",\n\t\t\"title\":\"Weather Underground\",\n\t\t\"link\":\"http://www.wunderground.com\"\n\t\t},\n\t\t\"display_location\": {\n\t\t\"full\":\"Warsaw, Poland\",\n\t\t\"city\":\"Warsaw\",\n\t\t\"state\":\"\",\n\t\t\"state_name\":\"Poland\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"zip\":\"00000\",\n\t\t\"magic\":\"1\",\n\t\t\"wmo\":\"12375\",\n\t\t\"latitude\":\"52.16999817\",\n\t\t\"longitude\":\"20.96999931\",\n\t\t\"elevation\":\"107.00000000\"\n\t\t},\n\t\t\"observation_location\": {\n\t\t\"full\":\"KÅ\u0082obucka, Warszawa, \",\n\t\t\"city\":\"KÅ\u0082obucka, Warszawa\",\n\t\t\"state\":\"\",\n\t\t\"country\":\"PL\",\n\t\t\"country_iso3166\":\"PL\",\n\t\t\"latitude\":\"52.167099\",\n\t\t\"longitude\":\"20.992540\",\n\t\t\"elevation\":\"0 ft\"\n\t\t},\n\t\t\"estimated\": {\n\t\t},\n\t\t\"station_id\":\"IWARSZAW528\",\n\t\t\"observation_time\":\"Last Updated on August 31, 3:34 PM CEST\",\n\t\t\"observation_time_rfc822\":\"Wed, 31 Aug 2016 15:34:17 +0200\",\n\t\t\"observation_epoch\":\"1472650457\",\n\t\t\"local_time_rfc822\":\"Wed, 31 Aug 2016 15:37:35 +0200\",\n\t\t\"local_epoch\":\"1472650655\",\n\t\t\"local_tz_short\":\"CEST\",\n\t\t\"local_tz_long\":\"Europe/Warsaw\",\n\t\t\"local_tz_offset\":\"+0200\",\n\t\t\"weather\":\"Scattered Clouds\",\n\t\t\"temperature_string\":\"76.3 F (24.6 C)\",\n\t\t\"temp_f\":76.3,\n\t\t\"temp_c\":24.6,\n\t\t\"relative_humidity\":\"47%\",\n\t\t\"wind_string\":\"From the SW at 2.5 MPH Gusting to 5.0 MPH\",\n\t\t\"wind_dir\":\"SW\",\n\t\t\"wind_degrees\":225,\n\t\t\"wind_mph\":2.5,\n\t\t\"wind_gust_mph\":\"5.0\",\n\t\t\"wind_h\":4.0,\n\t\t\"wind_gust_kph\":\"8.0\",\n\t\t\"pressure_mb\":\"1024\",\n\t\t\"pressure_in\":\"30.24\",\n\t\t\"pressure_trend\":\"0\",\n\t\t\"dewpoint_string\":\"55 F (13 C)\",\n\t\t\"dewpoint_f\":55,\n\t\t\"dewpoint_c\":13,\n\t\t\"heat_index_string\":\"NA\",\n\t\t\"heat_index_f\":\"NA\",\n\t\t\"heat_index_c\":\"NA\",\n\t\t\"windchill_string\":\"NA\",\n\t\t\"windchill_f\":\"NA\",\n\t\t\"windchill_c\":\"NA\",\n\t\t\"feelslike_string\":\"76.3 F (26 C)\",\n\t\t\"feelslike_f\":\"76.3\",\n\t\t\"feelslike_c\":\"26\",\n\t\t\"visibility_mi\":\"6.2\",\n\t\t\"visibility_km\":\"10.0\",\n\t\t\"solarradiation\":\"--\",\n\t\t\"UV\":\"3\",\"precip_1hr_string\":\"-999.00 in ( 0 mm)\",\n\t\t\"precip_1hr_in\":\"-999.00\",\n\t\t\"precip_1hr_metric\":\" 0\",\n\t\t\"precip_today_string\":\"0.00 in (0 mm)\",\n\t\t\"precip_today_in\":\"0.00\",\n\t\t\"precip_today_metric\":\"0\",\n\t\t\"icon\":\"partlycloudy\",\n\t\t\"icon_url\":\"http://icons.wxug.com/i/c/k/partlycloudy.gif\",\n\t\t\"forecast_url\":\"http://www.wunderground.com/global/stations/12375.html\",\n\t\t\"history_url\":\"http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=IWARSZAW528\",\n\t\t\"ob_url\":\"http://www.wunderground.com/cgi-bin/findweather/getForecast?query=52.167099,20.992540\",\n\t\t\"nowcast\":\"\"\n\t}\n}";
    
    WDParser parser = new JsonWdParser();
    parser.parseWD(jsonString);

  }

  @Test(expected = InvalidParameterException.class)
  public void testThrowsExceptionOnEmptyJson() throws JSONException, Exception {
    String jsonString = "";
    WDParser parser = new JsonWdParser();
    parser.parseWD(jsonString);
  }


}
