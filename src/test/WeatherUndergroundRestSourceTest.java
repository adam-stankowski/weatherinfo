package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;

import tomatosir.weather.ConfigManager;
import tomatosir.weather.WDSource;
import tomatosir.weather.WeatherUndergroundRestSource;

public class WeatherUndergroundRestSourceTest {
  private static ConfigManager configManager;

  @BeforeClass
  public static void beforeClass() throws IOException {
    configManager = new ConfigManager("test.properties");
  }

  @Test
  public void testSourceReturnsAString() throws Exception {
    WDSource source = new WeatherUndergroundRestSource(
        configManager.getConnectionString());
    assertNotNull(source.get());
  }

  @Test
  public void testThrowsIOExceptionOnWrongUrl() throws Exception {
    WDSource source = new WeatherUndergroundRestSource("wrong-url");
    Optional<Throwable> exc = tryGetResponse(source);
    assertTrue(exc.isPresent());
    assertEquals(MalformedURLException.class, exc.get().getClass());
  }

  private Optional<Throwable> tryGetResponse(WDSource source) throws Exception {
    try {
      String s = source.get();
      return Optional.empty();
    } catch (MalformedURLException e) {
      return Optional.of(e);
    }
  }

}
