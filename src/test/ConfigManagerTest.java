package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.junit.BeforeClass;
import org.junit.Test;

import tomatosir.weather.ConfigManager;

public class ConfigManagerTest {
  private static ConfigManager configManager;
  
  @BeforeClass
  public static void setup() throws IOException{
    configManager = new ConfigManager("test.properties");
  }

  @Test
  public void testConfigManager() throws IOException {
    ConfigManager config = new ConfigManager("test.properties");
  }

  @Test(expected = InvalidParameterException.class)
  public void testConfigManagerFileDoesNotExist() throws IOException {
    ConfigManager config = new ConfigManager("configure.properties");
  }

  @Test
  public void testGetConnectionString() throws IOException {

    assertNotNull(configManager.getConnectionString());
  }

  @Test
  public void testGetDBMS() throws IOException {
    assertEquals("mysql", configManager.getDBMS());
  }

  @Test
  public void testgetDBServerName() throws IOException {
    assertEquals("localhost", configManager.getDBServerName());
  }

  @Test
  public void testgetDBPort() throws IOException {
    assertEquals("3306", configManager.getDBPort());
  }

  @Test
  public void testgetDBName() throws IOException {
    assertEquals("weatherinfo_test", configManager.getDBName());
  }

  @Test
  public void testgetDBPassword() throws IOException {
    assertNotNull(configManager.getDBPassword());
  }

  @Test
  public void testgetDBUserName() throws IOException {
    assertNotNull(configManager.getDBUserName());
  }

  @Test
  public void testGetSaveFileName() throws IOException {
    assertEquals("weather_data_test.csv", configManager.getSaveFileName());
  }
  
  @Test
  public void testSaveToFile() throws IOException {
    assertTrue(configManager.saveToFile());
  }
  @Test
  public void testSaveToDB() throws IOException {
    assertTrue(configManager.saveToDB());
  }

}
