package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.BeforeClass;
import org.junit.Test;

import tomatosir.weather.CsvWDFormatter;
import tomatosir.weather.WeatherData;

public class CsvWDFormatterTest {
  private static WeatherData correctWeatherData;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    ZonedDateTime date = ZonedDateTime.of(2016, Month.DECEMBER.getValue(), 15, 21,
        45, 37, 0, ZoneId.ofOffset("", ZoneOffset.ofHours(1)));
    correctWeatherData = new WeatherData.WeatherDataBuilder().dateOfMeasurement(date)
        .feelsLikeTempCelsius(0).humidity(89).pressure(1034).pressureTrend("0")
        .tempCelsius(0.2).visibilityKM(10).windKPH(1.0).build();
  }

  @Test
  public void testReturnsCorrectStringOnCorrectObject() {

    String expected = "2016-12-15T21:45:37+01:00,0.2,1.0,0.0,10.0,1034,89,0";

    assertEquals(expected, CsvWDFormatter.format(correctWeatherData));

  }
  
  @Test(expected=NullPointerException.class)
  public void testThrowsExceptionOnNullWeatherData() {

    CsvWDFormatter.format(null);
  }

}
