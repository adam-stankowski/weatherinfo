/**
 * 
 */
package tomatosir.weather;

/**
 * @author Adam Stankowski
 *  Interface to represent classes which parse
 *  Weather Data objects from various String representations
 */
public interface WDParser {
  WeatherData parseWD(String s) throws Exception;
}
