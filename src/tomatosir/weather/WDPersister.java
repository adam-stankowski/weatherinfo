package tomatosir.weather;

import java.util.logging.Logger;

/**
 * Interface for classes which want to persist WeatherData objects
 * in any medium - currently database or file
 * @author Adam Stankowski
 *
 */
public interface WDPersister {
  void persist(WeatherData weatherData, Logger logger) throws Exception;
}
