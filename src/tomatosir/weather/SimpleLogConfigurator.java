/**
 * 
 */
package tomatosir.weather;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Adam Stankowski This class is a simplified logger, using Logger instance
 *         to log messages
 */
public class SimpleLogConfigurator {

  public static Logger configureLogger(String className) {
    Logger newLogger = Logger.getLogger(className);
    FileHandler fileHandler;
    SimpleFormatter formatter;

    try {
      formatter = new SimpleFormatter();
      fileHandler = new FileHandler("mylog.txt", true);
      fileHandler.setFormatter(formatter);
      newLogger.addHandler(fileHandler);
      newLogger.setLevel(Level.ALL);
    } catch (SecurityException e1) {
      e1.printStackTrace();
    } catch (IOException e1) {
      e1.printStackTrace();
    }

    return newLogger;
  }

}
