package tomatosir.weather;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class JDBCUtilTest {

  static ConfigManager c;
  static JDBCUtil u;
  static Connection con;

  public JDBCUtilTest() throws IOException, SQLException {
    // c = new ConfigManager("config.properties");
    // u = new JDBCUtil(c);
    // con = u.getConnection();
  }

  @BeforeClass
  public static void open() throws IOException, SQLException {
    c = new ConfigManager("test.properties");
    u = new JDBCUtil(c);
    con = u.getConnection();
  }

  @AfterClass
  public static void close() throws SQLException {
    u.closeConnection(con);
  }

  @Test
  public void createTableTest() throws IOException, SQLException {

    WeatherDataTable.createTable(con);
  }

  @Test
  public void tableExistsTest() throws IOException, SQLException {

    assertTrue(WeatherDataTable.tableExists(con));
  }

  @Test
  public void insertRowTest() throws SQLException {
    ZonedDateTime pDateTime = ZonedDateTime.parse("Wed, 31 Aug 2016 15:34:17 +0200",
        DateTimeFormatter.ofPattern("EEE, dd MMM yyyy kk:mm:ss x"));
    // YYYY-MM-DD HH:MM:SS
    WeatherData wData = new WeatherData.WeatherDataBuilder().tempCelsius(19.3)
        .feelsLikeTempCelsius(18.5).humidity(10).pressure(1013).visibilityKM(10.5)
        .windKPH(0.5).pressureTrend("-").dateOfMeasurement(pDateTime).build();

    WeatherDataTable.insertRow(wData, con);
  }

  @Test
  public void batchInsertTest() {
    ArrayList<WeatherData> l = new ArrayList<>();
    for (int i = 0; i < 30; i++) {
      l.add(new WeatherData.WeatherDataBuilder().tempCelsius(19.3 + i)
          .feelsLikeTempCelsius(18.5 + i).humidity(i).pressure(1000 + i)
          .visibilityKM(10.5).windKPH(0.5).pressureTrend("-")
          .dateOfMeasurement(ZonedDateTime.now()).build());
    }
    try {
      WeatherDataTable.batchInsert(l, con);
    } catch (SQLException e) {

      if (con != null) {
        try {
          System.err.println("Rolling back the transaction\n"+e.getMessage());
          con.rollback();
        } catch (SQLException ex) {
          System.err.println("Rollback unsuccessful\n"+ex.getMessage());
        }
      }
    }
  }

}
