package tomatosir.weather;

public interface Builder<Type> {
  Type build();
}
