/**
 * 
 */
package tomatosir.weather;

import java.util.Optional;

/**
 * @author Adam Stankowski This is an interface for objects which represent a source
 *         of Weather Data with a get method to fetch a particular WeatherData in a
 *         string form.
 */
public interface WDSource {
  String get() throws Exception;
}
