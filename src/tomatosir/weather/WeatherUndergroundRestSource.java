package tomatosir.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public final class WeatherUndergroundRestSource implements WDSource {
  private final String url;
  
  
  /**
   * Constructs WeatherUndergroundRestSource object. 
   * @param url a url to REST api from which data is fetched
   */
  public WeatherUndergroundRestSource(String url) {
    this.url = url;
  }
  
  /**
   * Gets WeatherData in string form from WeatherUnderground
   * REST api by opening a connection specified by URL and 
   * reading from the stream
   * @throws IOException 
   */
  @Override public String get() throws IOException {
    URL wundergroundApiURL = new URL(this.url);
    URLConnection connection = wundergroundApiURL.openConnection();
    StringBuilder result = new StringBuilder();
    try (BufferedReader bufferedReader = new BufferedReader(
        new InputStreamReader(connection.getInputStream()))) {
      String inputLine;
      while ((inputLine = bufferedReader.readLine()) != null) {
        result.append(inputLine);
      }
    }
    return result.toString();
  }

}
