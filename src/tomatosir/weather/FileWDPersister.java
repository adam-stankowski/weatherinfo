/**
 * 
 */
package tomatosir.weather;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * @author Adam Stankowski
 * This class is responsible for saving WeatherData object in 
 * a file on disk
 *
 */
public final class FileWDPersister implements WDPersister {
  private final ConfigManager configManager;

  /**
   * Constructs FileWDPersister object
   * 
   * @param filename
   *          - name of a file to save WeatherData information to
   */
  public FileWDPersister(ConfigManager configManager) {
    this.configManager = Objects.requireNonNull(configManager);
  }

  /**
   * Persists WeatherData objects passed as a parameter in a file on disk
   * 
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Override
  public void persist(WeatherData weatherData, Logger logger)
      throws FileNotFoundException, IOException {

    String filename = configManager.getSaveFileName();
    weatherData = Objects.requireNonNull(weatherData);
    logger = Objects.requireNonNull(logger);

    if (filename.isEmpty()) {
      throw new InvalidParameterException(
          "Filename to save WeatherData to is empty");
    }

    boolean headerExists = checkIfCSVFileHeaderExists(filename);

    try (PrintWriter printWriter = new PrintWriter(new FileWriter(filename, true))) {

      if (!headerExists) {
        printWriter
            .println("DateOfMeasurement,TempCelsius,WindKPH,FeelsLikeTempCelsius,"
                + "VisibilityKM,Pressure,Humidity,PressureTrend");
      }

      printWriter.println(CsvWDFormatter.format(weatherData));

      logger.info("Successfully saved WeatherData to file");

    }

  }
  
  /**
   * Verifies whether a CSV header exists in the file to write to
   * 
   * @param filenameToCheck - name of the file to verify for header existence
   * @return True if the header exists, otherwise false
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static boolean checkIfCSVFileHeaderExists(String filenameToCheck)
      throws FileNotFoundException, IOException {

    if (!Files.exists(Paths.get(filenameToCheck), LinkOption.NOFOLLOW_LINKS)) {
      return false;
    }

    boolean result = false;

    try (BufferedReader inputStream = new BufferedReader(
        new FileReader(filenameToCheck))) {
      result = inputStream.lines()
          .anyMatch((s) -> s
              .equals("DateOfMeasurement,TempCelsius,WindKPH,FeelsLikeTempCelsius,"
                  + "VisibilityKM,Pressure,Humidity,PressureTrend"));
      /*
       * String line = inputStream.readLine(); if (line != null && line.equals(
       * "DateOfMeasurement,TempCelsius,WindKPH,FeelsLikeTempCelsius," +
       * "VisibilityKM,Pressure,Humidity,PressureTrend")) { result = true; }
       */
    }
    return result;
  }

}
