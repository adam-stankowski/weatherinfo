package tomatosir.weather;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

public final class DatabaseWDPersister implements WDPersister {
  private final ConfigManager configManager;

  /**
   * Constructs a DatabaseWDPersister object
   * 
   * @param configManager
   *          - a cinfiguration manager object with database connection details
   */
  public DatabaseWDPersister(ConfigManager configManager) {
    this.configManager = configManager;
  }

  @Override
  public void persist(WeatherData weatherData, Logger logger) throws SQLException {
    JDBCUtil util = new JDBCUtil(configManager);

    try (Connection con = util.getConnection()) {
      logger.info("Successfully connected to database");
      if (!WeatherDataTable.tableExists(con)) {
        logger.info("Creating weatherdata database table");
        WeatherDataTable.createTable(con);
      }
      WeatherDataTable.insertRow(weatherData, con);
      logger.info("Successfully inserted WeatherData into database");
    }
  }

}
