package tomatosir.weather;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import org.json.JSONException;

/**
 * This class is a main application which fetches data from WeatherUnderground public
 * API and stores weather info in file or database dependent on configuration file
 * settings
 * 
 * @author Adam Stankowski
 *
 */
public class WeatherInfo {

  public static void main(String[] args) {

    WeatherInfo weatherInfo = new WeatherInfo();

    // If something failed we better log it. Logfile name hardcoded
    Logger logger = SimpleLogConfigurator
        .configureLogger(weatherInfo.getClass().getName());

    // Read properties file with config data
    ConfigManager configManager = weatherInfo.createConfigManager(logger);

    List<WDPersister> persisters = weatherInfo.configurePersisters(configManager);

    WeatherData weatherData = weatherInfo.getWDFromWeb(configManager, logger);

    try {

      for (WDPersister persister : persisters) {
        persister.persist(weatherData, logger);
      }
    } catch (Exception e) {
      logger.warning("Could not persist data " + e.getMessage());
    }
  }

  private List<WDPersister> configurePersisters(ConfigManager configManager) {
    Objects.requireNonNull(configManager);

    List<WDPersister> persisters = new ArrayList<>();
    if (configManager.saveToFile()) {
      persisters.add(new FileWDPersister(configManager));
    }
    if (configManager.saveToDB()) {
      persisters.add(new DatabaseWDPersister(configManager));
    }

    return persisters;
  }

  /**
   * Creates a ConfigManager object which stores config data for our app
   * 
   * @param logger
   *          used to log issues
   * @return
   */
  private ConfigManager createConfigManager(Logger logger) {
    ConfigManager configManager = null;

    try {
      configManager = new ConfigManager("config.properties");

    } catch (IOException e1) {
      logger.warning("Could not read config file " + e1.getMessage());
      System.exit(0);
    } catch (InvalidParameterException e) {
      logger.warning("Could not find config file " + e.getMessage());
      System.exit(0);
    }

    return configManager;
  }

  /**
   * Fetches weather data from Weatherunderground API and parses it to WeatherData
   * object
   * 
   * @param configManager
   *          configuration file used
   * @param logger
   *          used to log issues
   * @return
   */
  private WeatherData getWDFromWeb(ConfigManager configManager, Logger logger) {
    String response;
    WeatherData wd = null;
    try {

      WDSource source = new WeatherUndergroundRestSource(
          configManager.getConnectionString());
      response = source.get();
      logger.info("Fetched response from server successfully");
      wd = new JsonWdParser().parseWD(response);
      logger.info("Successfully Converted server response to Weather Data");

    } catch (MalformedURLException e) {
      System.err.println("URL for fetching was incorrect");
      logger.warning("URL for fetching was incorrect " + e.getMessage());
    } catch (IOException e) {
      System.err.println("Could not read data from Weather Underground API");
      logger.warning(
          "Could not read data from Weather Underground API " + e.getMessage());
    } catch (JSONException e) {
      System.err.println(
          "Fetched JSON was not formatted properly. Error while parsing JSON");
      logger.warning(
          "Fetched JSON was not formatted properly. Error while parsing JSON "
              + e.getMessage());
    } catch (Exception e) {
      logger.warning(
          "Unknown error while fetching weather data from web " + e.getMessage());
    }

    return wd;
  }

}
