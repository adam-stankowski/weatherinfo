package tomatosir.weather;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {
  private String dbms, dbName, dbUser, dbPassword, dbServer, dbPortNumber;

  public JDBCUtil(ConfigManager config) {
    dbms = config.getDBMS();
    dbName = config.getDBName();
    dbUser = config.getDBUserName();
    dbPassword = config.getDBPassword();
    dbServer = config.getDBServerName();
    dbPortNumber = config.getDBPort();
  }

  /**
   * Returns full database connection string to be used by 
   * DriverManager.getConnection
   * 
   * @return
   */
  private String getDBConectionString() {
    return String.format("jdbc:%s://%s:%s/%s", dbms, dbServer, dbPortNumber, dbName);
  }

  /**
   * Prepares the JDBC Connection object according to required properties
   * 
   * @return Connection object created that can be used to execute SQL statements
   * @throws SQLException
   */
  public Connection getConnection() throws SQLException {

    return DriverManager.getConnection(this.getDBConectionString(), dbUser,
        dbPassword);
  }
  
  /**
   * Closes a connection to database.
   * @param con Connection object to close
   * @throws SQLException
   */
  public void closeConnection(Connection con) throws SQLException {
    if (con != null) {
      con.close();
      con = null;
    }
  }

}
