package tomatosir.weather;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Properties;

/**
 * Immutable class which helps fetch configuration details from .properties
 * file
 * @author Adam Stankowski
 *
 */
public class ConfigManager {
  private final InputStream inputStream;
  private final Properties properties;

  public ConfigManager(String propFileName) throws IOException {
    properties = new Properties();

    inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

    if (inputStream != null) {
      properties.load(inputStream);
    } else {
      throw new InvalidParameterException(propFileName);
    }
  }

  /**
   * Returns a connection sString to Wundergound API read from Properties file
   * 
   * @return String representing the conection string
   */
  public String getConnectionString() {
    return new String(properties.getProperty("connectionString"));
  }

  /**
   * Returns a DBMS name from properties file
   * 
   * @return
   */
  public String getDBMS() {
    return new String(properties.getProperty("dbms"));
  }

  /**
   * Returns the DBMS Driver name from properties file.
   * 
   * @return
   */
  public String getDBServerName() {
    return new String(properties.getProperty("dbServer"));
  }

  /**
   * Returns Database username
   * 
   * @return
   */
  public String getDBUserName() {
    return new String(properties.getProperty("dbUser"));
  }

  /**
   * Returns the database spassword from Properties file
   * 
   * @return
   */
  public String getDBPassword() {
    return new String(properties.getProperty("dbPassword"));
  }

  /**
   * Returns database port number required for connection
   * 
   * @return
   */
  public String getDBPort() {
    return new String(properties.getProperty("dbPortNumber"));
  }

  /**
   * Returns the database name from properties file
   * 
   * @return
   */
  public String getDBName() {
    return new String(properties.getProperty("dbName"));
  }

  /**
   * Returns a name of a file to save data to
   * @return File name
   */
  public String getSaveFileName() {
    return new String(properties.getProperty("saveFilename"));
  }
  
  /**
   * Confirms whether configuration states that Weather data should 
   * be saved to file
   * @return True if data should be saved to file, false otherwise
   */
  public boolean saveToFile() {
    return properties.getProperty("saveToFile").equals("1");
  }

  /**
   * Confirms whether configuration states that Weather data should 
   * be saved to database
   * @return True if data should be saved to database, false otherwise
   */
  public boolean saveToDB() {
    return properties.getProperty("saveToDB").equals("1");
  }
}
