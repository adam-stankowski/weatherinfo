package tomatosir.weather;

import java.security.InvalidParameterException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is an implementation of WDParser which parses data from string in a form of
 * JSON
 * 
 * @author Adam Stankowski
 *
 */
public final class JsonWdParser implements WDParser {

  /**
   * Parses WD data from weatherString which is a JSON string and returns WeatherData
   * object which is contained in this string. In case of issues with parsing a
   * JSONException is thrown
   */
  @Override
  public WeatherData parseWD(String weatherString) throws JSONException {
    if (weatherString == null
        || (weatherString != null && weatherString.isEmpty())) {
      throw new InvalidParameterException();
    }

    JSONObject fullJSONObject = new JSONObject(weatherString);
    JSONObject currentWeatherJSONObject = fullJSONObject
        .getJSONObject("current_observation");

    double tempCelsius, windKph, feelsLikeTempCelsius, visibilityKm;
    int pressure, humidity;
    String pressureTrend;
    ZonedDateTime dateOfMeasurement;

    tempCelsius = currentWeatherJSONObject.optDouble("temp_c", 0);
    windKph = currentWeatherJSONObject.optDouble("wind_kph", 0);
    feelsLikeTempCelsius = currentWeatherJSONObject.optDouble("feelslike_c", 0);
    visibilityKm = currentWeatherJSONObject.optDouble("visibility_km", 0);
    pressure = currentWeatherJSONObject.optInt("pressure_mb", 0);
    humidity = Integer.parseInt(currentWeatherJSONObject
        .optString("relative_humidity", "N/A").replace("%", ""));
    pressureTrend = currentWeatherJSONObject.optString("pressure_trend", "N/A");
    dateOfMeasurement = parseDateTime(
        currentWeatherJSONObject.getString("observation_time_rfc822"));

    return new WeatherData.WeatherDataBuilder().dateOfMeasurement(dateOfMeasurement)
        .feelsLikeTempCelsius(feelsLikeTempCelsius).humidity(humidity)
        .pressure(pressure).pressureTrend(pressureTrend).tempCelsius(tempCelsius)
        .visibilityKM(visibilityKm).windKPH(windKph).build();
  }

  private static ZonedDateTime parseDateTime(String dateTimeInfo) {
    // Wed, 31 Aug 2016 15:34:17 +0200
    DateTimeFormatter formatter = DateTimeFormatter
        .ofPattern("EEE, dd MMM yyyy kk:mm:ss x");
    ZonedDateTime result;
    try {
      result = ZonedDateTime.parse(dateTimeInfo, formatter);
    } catch (DateTimeParseException e) {
      result = ZonedDateTime.now(ZoneId.systemDefault()); // I'm assuming measurement
                                                          // is taking place in
                                                          // Warsaw
    }

    return result;
  }

}
