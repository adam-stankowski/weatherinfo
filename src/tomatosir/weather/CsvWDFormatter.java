/**
 * 
 */
package tomatosir.weather;

/**
 * @author Adam Stankowski This class formats WeatherData object to CSV form
 */
public final class CsvWDFormatter {

  public static String format(WeatherData weatherData) {
    return String.format("%s,%.1f,%.1f,%.1f,%.1f,%d,%d,%s",
        weatherData.getDateOfMeasurement().toString(), weatherData.getTempCelsius(),
        weatherData.getWindKph(), weatherData.getFeelsLikeTempCelsius(),
        weatherData.getVisibilityKm(), weatherData.getPressure(),
        weatherData.getHumidity(), weatherData.getPressureTrend());
  }

}
