package tomatosir.weather;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class WeatherDataTable {

  /**
   * Checks if weatherinfo_data exists in the database 
   * This is to avoid sending create table request
   * over and over again if that table exists already
   * 
   * @param con
   *          SQL connection object
   * @return True if table already exists
   * @throws SQLException
   */
  public static boolean tableExists(Connection con) throws SQLException {
    String sql = "SHOW TABLES LIKE 'weatherinfo_data';";
    boolean result = false;
    try (Statement s = con.createStatement()) {
      ResultSet rs = s.executeQuery(sql);
      if (rs.next()) {
        rs.getString(1);
        result = !rs.wasNull();
      }
    }

    return result;
  }

  /**
   * This function creates weatherinfo_data table 
   * in case it's not present in the database
   * 
   * @param con
   *          SQL connection object
   * @throws SQLException
   */
  public static void createTable(Connection con) throws SQLException {
    String sql = new StringBuilder()
        .append("CREATE TABLE IF NOT EXISTS weatherinfo_data (")
        .append("id INT NOT NULL PRIMARY KEY auto_increment,")
        .append("temp_celsius FLOAT NOT NULL,")
        .append("feels_like_temp_celsius FLOAT NOT NULL,")
        .append("wind_kph FLOAT NOT NULL,").append("visibility_km FLOAT NOT NULL,")
        .append("pressure INT NOT NULL,").append("humidity INT NOT NULL,")
        .append("pressure_trend VARCHAR(3) NULL,")
        .append("measured_date_utc DATETIME NOT NULL);").toString();

    try (Statement s = con.createStatement()) {
      s.executeUpdate(sql);
    }
  }

  /**
   * Inserts a single record to weatherinfo_data table 
   * Date of measurement is stored in database in
   * UTC format to avoid ambiguity when retrieving it.
   * 
   * @param wData
   * @param con
   * @throws SQLException
   */
  public static void insertRow(WeatherData wData, Connection con)
      throws SQLException {
    // YYYY-MM-DD HH:MM:SS
    String sql = String.format(
        "INSERT INTO weatherinfo_data "
            + "(temp_celsius,feels_like_temp_celsius,wind_kph,visibility_km,"
            + "pressure,humidity,pressure_trend,"
            + "measured_date_utc) VALUES (%.1f,%.1f,%.1f,%.1f,%d,%d,'%s','%s');",
        wData.getTempCelsius(), wData.getFeelsLikeTempCelsius(), wData.getWindKph(),
        wData.getVisibilityKm(), wData.getPressure(), wData.getHumidity(),
        wData.getPressureTrend(),
        wData.getDateOfMeasurement().withZoneSameInstant(ZoneOffset.UTC)
            .format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")));

    try (Statement s = con.createStatement()) {
      s.executeUpdate(sql);
    }

  }

  private static void setParametersForInsert(PreparedStatement ps, WeatherData w)
      throws SQLException {
    // Narrowing down conversion, should be ok here.
    // Possible loss of precision is not of concern

    ps.setFloat(1, (float) w.getTempCelsius());
    ps.setFloat(2, (float) w.getFeelsLikeTempCelsius());
    ps.setFloat(3, (float) w.getWindKph());
    ps.setFloat(4, (float) w.getVisibilityKm());
    ps.setInt(5, w.getPressure());
    ps.setInt(6, w.getHumidity());
    ps.setString(7, w.getPressureTrend());
    ps.setString(8, w.getDateOfMeasurement().withZoneSameInstant(ZoneOffset.UTC)
        .format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")));

  }
  /**
   * Inserts several rows of weatherinfo_data items in a single transaction. 
   * IF any insert is unsuccessfull, the transaction is rolled back, but that
   * should be done from an external point. 
   * This function should probably catch its own exceptions so that 
   * the transaction can be rolled back in case it goes wrong, but I'm leaving
   * it for consistency with other methods.
   * @param wData Iterable WeatherData to be added
   * @param con SQL connection object
   * @throws SQLException
   */
  public static void batchInsert(Iterable<WeatherData> wData, Connection con)
      throws SQLException {
  
    String insertString = "INSERT INTO weatherinfo_data "
        + "(temp_celsius,feels_like_temp_celsius,wind_kph,visibility_km,"
        + "pressure,humidity,pressure_trend,"
        + "measured_date_utc) VALUES (?,?,?,?,?,?,?,?);";

    con.setAutoCommit(false);
    
    try (PreparedStatement ps = con.prepareStatement(insertString)) {

      for (WeatherData w : wData) {
        setParametersForInsert(ps, w);
        ps.executeUpdate(); 
      }
      con.commit(); 
    }
  }
}
