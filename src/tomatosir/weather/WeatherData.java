package tomatosir.weather;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Immutable class to present Weather Data
 * 
 * @author Adam Stankowski
 *
 */
public class WeatherData {
  private final double tempCelsius, windKph, feelsLikeTempCelsius, visibilityKm;
  private final int pressure, humidity;
  private final String pressureTrend;
  private final ZonedDateTime dateOfMeasurement;

  public WeatherData(WeatherDataBuilder b) {
    if (b.pressureTrend.length() > 3) { 
      throw new IllegalArgumentException(
          "Pressure trend cannot be longer than 3 characters");
    }

    this.tempCelsius = b.tempCelsius;
    this.windKph = b.windKph;
    this.feelsLikeTempCelsius = b.feelsLikeTempCelsius;
    this.visibilityKm = b.visibilityKm;
    this.pressure = b.pressure;
    this.humidity = b.humidity;
    this.pressureTrend = b.pressureTrend;
    this.dateOfMeasurement = b.dateOfMeasurement;
  }

  public double getTempCelsius() {
    return tempCelsius;
  }

  public double getWindKph() {
    return windKph;
  }

  public double getFeelsLikeTempCelsius() {
    return feelsLikeTempCelsius;
  }

  public double getVisibilityKm() {
    return visibilityKm;
  }

  public int getPressure() {
    return pressure;
  }

  public int getHumidity() {
    return humidity;
  }

  public String getPressureTrend() {
    return pressureTrend;
  }

  public ZonedDateTime getDateOfMeasurement() {
    return dateOfMeasurement;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((dateOfMeasurement == null) ? 0 : dateOfMeasurement.hashCode());
    long temp;
    temp = Double.doubleToLongBits(feelsLikeTempCelsius);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + humidity;
    result = prime * result + pressure;
    result = prime * result
        + ((pressureTrend == null) ? 0 : pressureTrend.hashCode());
    temp = Double.doubleToLongBits(tempCelsius);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(visibilityKm);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(windKph);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    WeatherData other = (WeatherData) obj;
    if (dateOfMeasurement == null) {
      if (other.dateOfMeasurement != null)
        return false;
    } else if (!dateOfMeasurement.equals(other.dateOfMeasurement))
      return false;
    if (Double.doubleToLongBits(feelsLikeTempCelsius) != Double
        .doubleToLongBits(other.feelsLikeTempCelsius))
      return false;
    if (humidity != other.humidity)
      return false;
    if (pressure != other.pressure)
      return false;
    if (pressureTrend == null) {
      if (other.pressureTrend != null)
        return false;
    } else if (!pressureTrend.equals(other.pressureTrend))
      return false;
    if (Double.doubleToLongBits(tempCelsius) != Double
        .doubleToLongBits(other.tempCelsius))
      return false;
    if (Double.doubleToLongBits(visibilityKm) != Double
        .doubleToLongBits(other.visibilityKm))
      return false;
    if (Double.doubleToLongBits(windKph) != Double.doubleToLongBits(other.windKph))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "WeatherData [tempCelsius=" + tempCelsius + ", windKph=" + windKph
        + ", feelsLikeTempCelsius=" + feelsLikeTempCelsius + ", visibilityKm="
        + visibilityKm + ", pressure=" + pressure + ", humidity=" + humidity
        + ", pressureTrend=" + pressureTrend + ", dateOfMeasurement="
        + dateOfMeasurement + "]";
  }
 
  /**
   * WeatherDataBuilder class is used to assist in building 
   * WeatherData objects as the contain many
   * parameters. It is helpful to make building of this 
   * object more transparent. Building
   * WeatherData with constructor with so many parameters is very error prone.
   * 
   * @author Adam Stankowski
   *
   */
  public static class WeatherDataBuilder implements Builder<WeatherData> {
    private double tempCelsius = 0;
    private double windKph = 0;
    private double feelsLikeTempCelsius = 0;
    private double visibilityKm = 0;
    private int pressure = 0;
    private int humidity = 0;
    private String pressureTrend = "0";
    private ZonedDateTime dateOfMeasurement = ZonedDateTime.now(ZoneId.of("+2"));

    public WeatherDataBuilder() {
    }

    @Override
    public WeatherData build() {
      return new WeatherData(this);
    }

    public WeatherDataBuilder tempCelsius(double temp) {
      this.tempCelsius = temp;
      return this;
    }

    public WeatherDataBuilder windKPH(double wind) {
      this.windKph = wind;
      return this;
    }

    public WeatherDataBuilder feelsLikeTempCelsius(double feelsLikeTemp) {
      this.feelsLikeTempCelsius = feelsLikeTemp;
      return this;
    }

    public WeatherDataBuilder visibilityKM(double visibility) {
      this.visibilityKm = visibility;
      return this;
    }

    public WeatherDataBuilder pressure(int p) {
      this.pressure = p;
      return this;
    }

    public WeatherDataBuilder humidity(int h) {
      this.humidity = h;
      return this;
    }

    public WeatherDataBuilder pressureTrend(String pT) {
      this.pressureTrend = pT;
      return this;
    }

    public WeatherDataBuilder dateOfMeasurement(ZonedDateTime dom) {
      this.dateOfMeasurement = dom;
      return this;
    }

  }

}
