# README #

This application reads weather data from Weatherunderground public API and stores it in database or file. On my machine (Ubuntu) I run a cron job to fire this application every 10 minutes (you can use Scheduled Tasks app for this). 
The simple process which this application follows is: 
* Setup logger to log any issues
* Read data from config file (Wunderground API url etc)
* Fetch data from Wunderground API
* Parse JSON Data
* Save in Database and/or file. 

## Important mechanisms included in this app are ##
* Using and setting up Java logger to log status messages and issues to file
* Reading config information from external .properties file
* Fetchinig data from Web API (WeatherDataHandler)
* Parsing JSON data to objects with org.json library (WeatherDataHandler)
* Saving data to a file (WeatherDataHandler)
* Fetching and inserting data to MySQL using JDBC
* Builder class for WeatherData class containing many constructor parameters (WeatherDataBuilder)

### What is this repository for? ###

This code comes with no guarantee whatsoever and should not be used for commercial purposes. It's here only for learning purposes, feel free to use the code snippets included here to develop your own applications, and if you find any better way of doing things, it would be perfect if you could let me know. 

### How do I get set up? ###

In order to set this project up, the only thing required should be to fork the repo and make sure you have a MySQL connector .jar file included in the build path. You may meed to build a MySQL database as the scripts are not included. Sample config file is included. 

### Testing ###

The .test package contains unit tests that I used to verify required java class functionality. For JDBCUtil testing I used a separate test database which was a copy of the original database, so that "production" data is not harmed. The connection to test database was retrieved in setUpBeforeClass() function and used for all the unit tests. 
I found this way of handling database tests quite convenient, but if you have any other approach to this, it would be great if you could let me know, always happy to learn :) 

### Who do I talk to? ###

* Repo owner or admin
